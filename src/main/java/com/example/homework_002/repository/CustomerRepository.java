package com.example.homework_002.repository;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("INSERT INTO customer (name, address, phone_number) VALUES (#{request.customerName}, #{request.customerAddress}, #{request.customerPhone})" + "RETURNING id")
    Integer addCustomer(@Param("request")CustomerRequest customerRequest);

    @Select("SELECT * FROM customer ORDER BY id ASC")
    List<Customer> getAllCustomers();

    @Select("SELECT * FROM customer WHERE id= #{customerId}")
    Customer getCustomerById(Integer customerId);

    @Select("UPDATE customer " +
            "SET name = #{request.customerName}, " +
            "address = #{request.customerAddress}, " +
            "phone_number = #{request.customerPhone} " +
            "WHERE id = #{id} " +
            "RETURNING id" )
    Integer updateCustomer(Integer id, @Param("request") CustomerRequest customerRequest);

    @Delete("DELETE FROM customer WHERE id = #{customerId} ")
    boolean deleteCustomer(Integer customerId);
}
