package com.example.homework_002.repository;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("INSERT INTO product (productname, productprice) VALUES (#{request.productName}, #{request.productPrice})"
            + "RETURNING id")
    Integer addProduct(@Param("request") ProductRequest productRequest);

    @Select("SELECT * FROM product ORDER BY id ASC")
    List<Product> getAllProducts();

    @Select("SELECT * FROM product WHERE id = #{id}")
    Product getProductById(Integer id);

    @Select("UPDATE product " +
            "SET productname = #{request.productName}, " +
            "productprice = #{request.productPrice} " +
            "WHERE id = #{id} " +
            "RETURNING id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer id);

    @Delete("DELETE FROM product WHERE id = #{id} ")
    Boolean deleteProduct(Integer id);
}

