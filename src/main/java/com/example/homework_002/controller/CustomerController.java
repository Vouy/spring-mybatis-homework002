package com.example.homework_002.controller;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import com.example.homework_002.model.response.CustomerResponse;
import com.example.homework_002.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/add-new-customer")
    @Operation(summary = "Add New Customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest) {
        Integer storeCustomerId = customerService.addCustomer(customerRequest);
        if (storeCustomerId != null) {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Add Customer Successfully")
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        } else {
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Sorry you can't add data")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
                    return ResponseEntity.ok(response);
        }
    }

    @GetMapping("/get-all-customer")
    @Operation(summary = "Get All Customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){
        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .message("Get All Customer")
                .payload(customerService.getAllCustomers())
                .httpStatus(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-customer-by-id/{id}")
    @Operation(summary = "Get Customer By Id")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){
        if(customerService.getCustomerById(customerId) != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Get Customer By Id Successfully")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Sorry! Search Customer Not Found")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }

    @PutMapping("/update-customer-by-id/{id}")
    @Operation(summary = "Update Customer By Id")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(@PathVariable("id") Integer customerId, @RequestBody CustomerRequest customerRequest){
        Integer updateCustomerId = customerService.updateCustomer(customerId,customerRequest);
        if(updateCustomerId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Update Customer Successfullty")
                    .payload(customerService.getCustomerById(customerId))
                    .httpStatus(true)
                    .build();
            return ResponseEntity.ok(response);
        }else{
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .message("Sorry ! You can't update")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }

    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    @Operation(summary = "Delete Customer By Id")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){
        boolean cusId = customerService.deleteCustomer(customerId);
        if(cusId == true){
            CustomerResponse<String> response = CustomerResponse.<String>builder()
                    .message("Delete Customer Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            CustomerResponse<String> response = CustomerResponse.<String>builder()
                    .message("Sorry you can't delete this customer")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }



}
