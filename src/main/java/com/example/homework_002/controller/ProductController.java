package com.example.homework_002.controller;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.model.response.ProductResponse;
import com.example.homework_002.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/add-product")
    public ResponseEntity<ProductResponse<Product>> addProduct(@RequestBody ProductRequest productRequest) {
        Integer productId = productService.addProduct(productRequest);
        if (productId != null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Add Product Successfully")
                    .payload(productService.getProductById(productId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Sorry you can't add new Product")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @GetMapping("/get-all-product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct() {
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .message("Get All Product")
                .payload(productService.getAllProducts())
                .httpStatus(true)
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .build();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId ) {
        Product product = productService.getProductById(productId);
        if (product != null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Get Product By Id Successfully")
                    .payload(productService.getProductById(productId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Search Product Not Found")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @PutMapping("/update-product/{id}")
    public ResponseEntity<ProductResponse<Product>> updateProduct(@RequestBody ProductRequest productRequest, @PathVariable("id") Integer productId) {
        Integer updateProId = productService.updateProduct(productRequest, productId);
        if (updateProId != null) {
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Update Product Successfully")
                    .payload(productService.getProductById(productId))
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .message("Sorry you can't Update")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }
    }

    @DeleteMapping("/delete-product/{id}")
    public ResponseEntity<ProductResponse<String>> deleteProduct(@PathVariable("id") Integer productId) {
        if (productService.deleteProduct(productId) == true) {
            ProductResponse<String> response = ProductResponse.<String>builder()
                    .message("Delete Product Successfully")
                    .httpStatus(true)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }else{
            ProductResponse<String> response = ProductResponse.<String>builder()
                    .message("Sorry you can't delete Product")
                    .httpStatus(false)
                    .timestamp(new Timestamp(System.currentTimeMillis()))
                    .build();
            return ResponseEntity.ok(response);
        }



    }






}
