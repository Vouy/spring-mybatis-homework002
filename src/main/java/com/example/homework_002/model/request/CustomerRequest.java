package com.example.homework_002.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class CustomerRequest {
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
