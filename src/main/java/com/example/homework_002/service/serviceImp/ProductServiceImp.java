package com.example.homework_002.service.serviceImp;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;
import com.example.homework_002.repository.ProductRepository;
import com.example.homework_002.service.ProductService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {
    private final ProductRepository productRepository;
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
    @Override
    public Integer addProduct(ProductRequest productRequest) {
        Integer productId = productRepository.addProduct(productRequest);
        return productId;
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer id) {
        return productRepository.updateProduct(productRequest, id);
    }

    @Override
    public boolean deleteProduct(Integer id) {
        return productRepository.deleteProduct(id);
    }
}
