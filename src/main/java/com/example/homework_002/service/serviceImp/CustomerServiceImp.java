package com.example.homework_002.service.serviceImp;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;
import com.example.homework_002.repository.CustomerRepository;
import com.example.homework_002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Integer addCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.addCustomer(customerRequest);
        return customerId;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Integer updateCustomer(Integer id, CustomerRequest customerRequest) {
        return customerRepository.updateCustomer(id, customerRequest);
    }

    @Override
    public boolean deleteCustomer(Integer id) {
        return customerRepository.deleteCustomer(id);
    }
}
