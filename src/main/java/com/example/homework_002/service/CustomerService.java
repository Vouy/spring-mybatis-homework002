package com.example.homework_002.service;

import com.example.homework_002.model.entity.Customer;
import com.example.homework_002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    Integer addCustomer(CustomerRequest customerRequest);

    List<Customer> getAllCustomers();

    Customer getCustomerById(Integer id);

    Integer updateCustomer(Integer id, CustomerRequest customerRequest);

    public boolean deleteCustomer(Integer id);
}
