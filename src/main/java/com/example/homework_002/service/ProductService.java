package com.example.homework_002.service;

import com.example.homework_002.model.entity.Product;
import com.example.homework_002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {
    Integer addProduct(ProductRequest productRequest);

    List<Product> getAllProducts();

    Product getProductById(Integer id);

    Integer updateProduct(ProductRequest productRequest, Integer id);

    public boolean deleteProduct(Integer id);
}
